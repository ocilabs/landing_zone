# return compartments
locals {
    activated = [for admin_domain, department in var.departments: admin_domain if department == true]
}

#locals {
  # flatten ensures that this local value is a flat list of objects, rather
  # than a list of lists of objects.
#  admin_memberships = flatten([
#    for user_key, admin in var.user_names: [
#      for group_key, role in user_names.membership : {
#        user_id = ...
#      }
#    ]
#  ])
#}