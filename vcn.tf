module "vcn" {
  source  = "oracle-terraform-modules/vcn/oci"
  version = "1.0.3"

  # deployment parameters
  region         = local.home_region
  compartment_id = var.tenancy_id
  label_prefix   = var.label_prefix

  # vcn configuration
  vcn_cidr                 = var.vcn_cidr
  vcn_dns_label            = "zone"
  vcn_name                 = "${var.label_prefix}_${var.platform_name}"
  internet_gateway_enabled = true
  nat_gateway_enabled      = true
  service_gateway_enabled  = true
  tags                     = {"Platform"=var.platform_name, "Environment"=var.label_prefix}
}