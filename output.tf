# return compartments
#output "resources" {
#    value = data.oci_identity_compartments.resources
#}
#output "services" {
#    value = data.oci_identity_compartments.services
#}

# return users
#output "user_details" {
#    value           = oci_identity_ui_password.user_secret
#}

# return groups
output "admins" {
    value = {
        for admin in oci_identity_group.admins:
        admin.name => admin.id
    }
}

# return network gateways
output "network" {
    value = {
        "vcn_id"         = module.vcn.vcn_id
        "ig_route_id"    = module.vcn.ig_route_id
        "nat_gateway_id" = module.vcn.nat_gateway_id
        "nat_route_id"   = module.vcn.nat_route_id
    }
}

# return activated compartments
output "admin_domains" {
    value = oci_identity_compartment.admin_domain
}