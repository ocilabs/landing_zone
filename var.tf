// retriev tenancy ocid
variable "compartment_ocid" {}

// naming conventions
variable "platform_name" { default = "name" }
variable "label_prefix"    { default = "dev" }

// define network
variable "vcn_cidr" { default = "10.0.0.0/16" }