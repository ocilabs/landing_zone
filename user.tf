resource "oci_identity_user" "admin_user" {
  provider       = oci.home
  for_each       = {for user_key in var.user_names:  user_key.name => user_key}

  #Required
  compartment_id = var.tenancy_id
  name           = each.key
  description    = each.value.description

  #Optional
  #defined_tags = {"Operations.CostCenter"= "42"}
  email          = each.value.email
  freeform_tags   = {"Platform"=var.platform_name, "Environment"=var.label_prefix}
}

resource "oci_identity_ui_password" "user_secret" {
  provider       = oci.home
  for_each       = {for user_key in var.user_names:  user_key.name => user_key}
  user_id        = oci_identity_user.admin_user[each.key].id
}

resource "oci_identity_user_capabilities_management" "admin_role" {
  provider                     = oci.home
  for_each                     = {for user_key in var.user_names:  user_key.name => user_key}
  user_id                      = oci_identity_user.admin_user[each.key].id
  can_use_api_keys             = false
  can_use_auth_tokens          = false
  can_use_console_password     = each.value.active
  can_use_customer_secret_keys = false
  can_use_smtp_credentials     = false
}

#resource "oci_identity_user_group_membership" "admin_role" {
  #Required
#  provider = oci.home
#  for_each = {for user_key in var.user_names:  user_key.name => user_key}
#  user_id  = oci_identity_user.admin_user[each.key].id
#  group_id = oci_identity_group.test_group.id
#}